const EventEmitter = require('events');
const http = require('http');
const mongoose = require('mongoose');
const url = require('url');
const Redis = require('ioredis');
const redis = new Redis();
const orderEmitter = new EventEmitter();
const BST = require('./bst'); require('./db');

const SIDES = [ 'buy', 'sell']
const hostname = '127.0.0.1';
const port = 3000;
const channel = 'order';

const bst = new BST();
const Schema = mongoose.Schema


/**
 * Order Schema
 */
const Order = new Schema({
    side: { type: String, enum: SIDES },
    price: { type: Number }
   }
);

const OrderModel = mongoose.model('Order', Order);


const addOrderToDataBase = async ({ side, price }) => {
  const order = new OrderModel();
  order.side = side;
  order.price = price;
  const orderSaved = await order.save();
  return orderSaved;
}

const addOrderToBinaryTree = ({ price }) => {
  bst.add(price);
}

const addOrderToRedis = ({_id, price}) => {
  const key = `key-${_id}`
  const value = price;
  redis.set(key, value)
}

const deleteTransactionsFromDB = async (transactionIds) => {
  await OrderModel.deleteMany({ _id: { $in: transactionIds } })
}

const deleteTransactionsFromBST = (prices) => {
    prices.forEach(price => bst.remove(price))
}

const deleteTransactionsFromRedis = (transactionIds) => {

  transactionIds.forEach(({ _id }) => {
    const key = `key-${_id}`
    redis.del(key);
  })
}


const checkMatchingPricesWithUnMatchSides = async ({ side, price }) => {
  const ordersFound = await OrderModel.find({  price : { $eq : price }, side: { $ne: side } });
  if(ordersFound){
    const transactionIds = ordersFound.map(order => order._id);
    const prices = ordersFound.map(order => order.price);

    await Promise.all([
      deleteTransactionsFromBST(prices),
      deleteTransactionsFromDB(transactionIds),
      deleteTransactionsFromRedis(transactionIds)
    ]);


  }
}

const logResults = async () => {
  const ordersFound = await OrderModel.find({ });
  const InOrderTraverseResult = bst.inOrder();

  console.log('Orders in Database', ordersFound);
  console.log('Order in Binary Tree', InOrderTraverseResult);

}



orderEmitter.on("order",   async (data) => {
  addOrderToDataBase(data);
  addOrderToBinaryTree(data);
  checkMatchingPricesWithUnMatchSides(data);
  logResults()
});

redis.subscribe(channel, (err, order) => {
  addOrderToRedis(order)
})

const server = http.createServer((req, res) => {

  const parsedUrl = url.parse(req.url, true); 
  const method = req.method;

  if(parsedUrl.pathname === '/order' && method === 'POST') {

    req.on('data', data => {
        const order = JSON.parse(data);

        Promise.all([orderEmitter.emit('order', order), redis.publish(channel, order) ]);
        res.writeHead(200, {'Content-Type': 'application/json'});
        res.end(JSON.stringify(order));
    })


  }
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});